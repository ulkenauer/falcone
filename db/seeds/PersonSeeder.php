<?php

use Phinx\Seed\AbstractSeed;

class PersonSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $pattern_data = file_get_contents(__DIR__ . '/patterns/MyPattern.json');
        $pattern = json_decode($pattern_data);

        //echo "a \n";
        echo "sample data: ";
        //echo $pattern['person']['firstnames'][0];
        $person = [];
        //$person = ['firstname' => $pattern->person->firstnames[rand(0,count($pattern->person->firstnames)-1)]];


        $regions = $this->fetchAll('SELECT id FROM region');
        $table = $this->table('person');

        {
            for ($i = 0; $i < 300000; $i++)
            {
                $person[] = [
                    'firstname' => $pattern->person->firstnames[rand(0, count($pattern->person->firstnames) - 1)],
                    'lastname' => $pattern->person->lastnames[rand(0, count($pattern->person->lastnames) - 1)],
                    'region_id' => $regions[rand(0, count($regions) - 1)][0]
                ];
            }
            echo $person[0]['firstname'], ' ', $person[0]['lastname'], ' ', $person[0]['region_id'] ;

            echo "\n";

            $table->insert($person)->save();
        }
    }
}
