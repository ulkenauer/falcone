<?php


use Phinx\Seed\AbstractSeed;


class RegionSeeder extends AbstractSeed
{

    private $pattern;
    /**
     * indexes - [tier] [region] [property]
     */
    private $generated;

    private $table;


    private function readPattern()
    {
        $pattern_data = file_get_contents(__DIR__ . '/patterns/MyPattern.json');
        $this->pattern = json_decode($pattern_data, true);
    }


    private function accessTable()
    {
        $this->table = $this->table('region');
    }

    /**
     * @param $tierNum
     * @throws Exception
     */

    private function generate($tierNum)
    {
        if(!array_key_exists('tier '.$tierNum, $this->pattern['regions']))
            throw new Exception('there is no tier '.$tierNum);
        $tier = $this->pattern['regions']['tier '.$tierNum];
        if(array_key_exists('second', $tier))
        {
            $ratio = count($tier['first'])/(count($tier['second']) + count($tier['first']));

            if(mt_rand() / mt_getrandmax() > $ratio)
            {
                $type = $tier['type'][mt_rand(0, count($tier['type'])-1)];
                $second = $tier['second'][mt_rand(0, count($tier['second'])-1)];
                $this->generated[$tierNum][] = ['name' => $type.' of '.$second, 'parent_id' => null];
                return;
            }
        }

        $type = $tier['type'][mt_rand(0, count($tier['type'])-1)];
        $first = $tier['first'][mt_rand(0, count($tier['first'])-1)];
        $this->generated[$tierNum][] = ['name' => $first.' '.$type, 'parent_id' => null];

    }

    /**
     * @return array
     */
    private function getRegionRoots()
    {
        return $this->fetchAll("select * from region where parent_id is null");
    }

    /**
     * @param $parentId
     * @return array
     * @throws Exception
     */

    private function getRegionChildren($parentId)
    {
        if(!is_int($parentId))
            throw(new Exception("parentId should be an integer number"));

        return $this->fetchAll("select * from region where parent_id = ".$parentId);
    }

    /**
     * @param $tier
     * @return array
     * @throws Exception
     */
    private function getRegionsOfTier($tier)
    {
        if(!is_int($tier) || $tier < 0)
            throw(new Exception("tier should be a positive integer number"));

        $result = $this->getRegionRoots();

        for($i = 0; $i < $tier; $i++)
        {
            if($result == null)
                return null;

//            if(count($result) == 0)
//                break;

            $children = null;

            foreach ($result as $item)
            {
                //echo count($result)." ".array_keys($result)[0]."\n";
                //echo $item['id']."\n";

                $tierChildren = $this->getRegionChildren(intval($item['id']));

                foreach ($tierChildren as $tierChild)
                {
                    $children[] = $tierChild;
                }

                //echo $children[0]['name'];

            }

            $result = $children;

        }

        return $result;
    }

    private function save()
    {
        $tier =& $this->generated[0];
        if(!empty($tier))
            $this->table->insert($tier)->save();

        for ($i = 1; $i < count($this->generated); $i++)
        {
            $tier =& $this->generated[$i];

            if(empty($tier))
                continue;

            $parents = $this->getRegionsOfTier($i-1);
            if(!empty($parents))
                foreach ($tier as &$region)
                {
                    $region['parent_id'] = intval($parents[mt_rand(0, count($parents)-1)]['id']);
                }

            $this->table->insert($tier)->save();
        }
    }

    public function run()
    {

        $this->accessTable();
        $this->readPattern();



        for($i = 0; $i < 5; $i++)
            $this->generate(0);
        for($i = 0; $i < 40; $i++)
            $this->generate(1);
        for($i = 0; $i < 40*10; $i++)
            $this->generate(2);

        for($i = 0; $i < 40*10*20; $i++)
            $this->generate(3);
        for($i = 0; $i < 40*10*20*30; $i++)
            $this->generate(4);

        $this->save();


        echo "\n";
    }
}
