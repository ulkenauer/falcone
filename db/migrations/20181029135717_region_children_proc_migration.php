<?php


use Phinx\Migration\AbstractMigration;

class RegionChildrenProcMigration extends AbstractMigration
{
    public function up()
    {
        $this->execute(file_get_contents(__DIR__ . '/001.sql'));
    }

    public function down()
    {
        $this->execute("DROP PROCEDURE IF EXISTS GetRegionChildren;");
    }
}
