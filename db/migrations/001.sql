DROP PROCEDURE IF EXISTS GetRegionChildren;
CREATE PROCEDURE GetRegionChildren(IN regionId INT)
  BEGIN
    drop temporary table if exists children;
    create temporary table children (id INT);

    drop temporary table if exists tier;
    create temporary table tier (id INT, parent_id INT);

    drop temporary table if exists ptier;
    create temporary table ptier (id INT, parent_id INT);

    insert into ptier (id, parent_id)
    select id, parent_id from region where id = regionId;

    label:loop
      insert into tier (id, parent_id)
      select id, parent_id from region where parent_id in (select id from ptier);

      if ((select count(*) from tier) = 0) then
        leave label;
      end if;

      insert into children (id)
      select id from ptier;
      delete from ptier;

      insert into ptier (id, parent_id)
      select id, parent_id from tier;
      delete from tier;
    end loop;

    insert into children (id)
    select id from ptier;

    drop temporary table if exists tier;
    drop temporary table if exists ptier;
  END;