<?php
/**
 * Created by PhpStorm.
 * User: appgrader
 * Date: 23.10.18
 * Time: 20:58
 */
use Phalcon\Mvc\Model;
class Region extends Model
{
    private $id;
    private $name;
    private $parent;

    public function getParent()
    {
        return $this->parent;
    }

    public function getChildren()
    {
        return Region::find('parent = ' . $this->id);
    }


    public function getDwellers()
    {
        return Person::find('region_id = '.$this->id);
    }

    public function getDwellerCount()
    {
        return Person::count('region_id = '.$this->id);
    }

    public function getChildrenExpanded($page, $pageSize)
    {

        $sql = 'call GetRegionChildren('.$this->id.');';
        $this->getReadConnection()->query($sql);
        //$sql = 'select * from region where id in (select id from children);';

        $sql = 'select * from region where id in (select id from children) and id != '.$this->getId().' limit '. $page*$pageSize .','. $pageSize . ';';

        $region = new Region();

        return new Model\Resultset\Simple($this->columnMap(), $region, $region->getReadConnection()->query($sql));
    }

    public function getDwellersExpanded($page, $pageSize)
    {
        $sql = 'call GetRegionChildren('.$this->id.');';
        $this->getReadConnection()->query($sql);
        $sql = 'select * from person where region_id in (select id from children) limit '. $page*$pageSize .','. $pageSize . ';';

        $person = new Person();

        return new Model\Resultset\Simple(null, $person, $person->getReadConnection()->query($sql));
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'parent_id' => 'parent'
        ];
    }
}