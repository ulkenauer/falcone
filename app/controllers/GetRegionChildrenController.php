<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class GetRegionChildrenController extends Controller
{
    public function indexAction()
    {
        $regionId = $this->request->getQuery()['region_id'];
        $page = intval($this->request->getQuery()['page']);

        try
        {
            $parentRegion = Region::find($regionId)[0];
        }
        catch (Exception $exception)
        {
            return new Response();
        }

        $pageSize = 100;

        $children = $parentRegion->getChildrenExpanded($page, $pageSize);
        $response = json_encode($children);
        return new Response($response);
    }
}