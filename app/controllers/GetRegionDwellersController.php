<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class GetRegionDwellersController extends Controller
{
    public function indexAction()
    {
        $regionId = $this->request->getQuery()['region_id'];
        $page = intval($this->request->getQuery()['page']);
        $pageSize = 100;

        try
        {
            $region = Region::find($regionId)[0];
            //$region = Region::findFirst($regionId);
        }
        catch (Exception $exception)
        {
            return new Response();
        }
        $dwellers = $region->getDwellersExpanded($page, $pageSize);
        $response = json_encode($dwellers);
        return new Response($response);
    }
}