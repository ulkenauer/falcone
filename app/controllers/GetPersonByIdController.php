<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class GetPersonByIdController extends Controller
{
    public function indexAction()
    {
        $personId = $this->request->getQuery()['person_id'];
        try
        {
            $person = Person::find($personId)[0];
        }
        catch (Exception $exception)
        {
            return new Response();
        }
        $data = json_encode($person);
        return new Response($data);
    }

}