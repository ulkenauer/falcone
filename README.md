# README

## Database

Firstly, create database with name 'development_db'.

### Credentials

1. Go to phinx.yml. Enter your credentials for each environment.

2. Go to public/index.php, find this block. Enter your database credentials as follows:

```php
    $di->set(
        'db',
        function () {
            return new DbAdapter(
                [
                    'host'     => '127.0.0.1',
                    'username' => 'username',
                    'password' => 'passwd',
                    'dbname'   => 'development_db',
                ]
            );
        }
    );
```

### Migrating

To make necessary migrations, simply use:

```bash
    ./phinx migrate -e development
```

### Seeding

To seed the database use:

```bash
    ./phinx seed:run -e development -s RegionSeeder
    ./phinx seed:run -e development -s PersonSeeder
```

1. Seed regions. Seeder makes parent bindings automatically. If database throws exception like 'maximum package limit exceeded' - try to generate regions carefully, tier after tier. Pay attention to the last tiers, because major part of regions are closer to tree leaves. Reduce amount of regions and run seeder several times.
2. Seed persons. Seeder adds 300k person records at once.