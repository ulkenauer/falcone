function updateStatusInfo(result)
{
    document.getElementById("result").setAttribute("value", result);
    //$("#result").value = status;
    //document.getElementById("result")
}
function getPostData()
{
    var data =
        {
            "var1": $("#var1").val(),
            "var2": $("#var2").val()
        };
    return JSON.stringify(data);
}
function load()
{
    $.post("calculator", getPostData(), updateStatusInfo);
}
function onStart()
{
    document.getElementById("load").onclick = load; //this one works too
    //$("#load").click(load); //with jQuery
}
$(document).ready(onStart);